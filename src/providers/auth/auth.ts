import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {HttpErrorResponse} from "../../../node_modules/@angular/common/http";
import {AlertController, LoadingController, NavController} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {MessagesPage} from "../../pages/messages/messages";
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

    loggedIn: boolean = false;
    user;
    registerStatus: boolean = false;
    apiUrl: string = "https://api.boep.finleyhd.nl/api/";

    constructor(
        private http: HttpClient,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private navCtrl: NavController,
        private storage: Storage

    ) {
        console.log('Authprovider initialized');
    }

    getUser() {
        this.storage.get('token').then((token => {
            if(token != null) {
                this.http.get(this.apiUrl + "users/" + token).subscribe((result: any) => {
                    this.user = result;
                    return result;
                })
            }
        }));

    }

    registerUser(username, password, email) {

        let loader = this.loadingCtrl.create({content: "Loading..", dismissOnPageChange: true});

        if (username.length < 1 || password.length < 1 || email.length < 1) {
            this.createAuthAlert("Error", "You have left some thing empty!", ['Okay'])
        }
        else {
            loader.present();

            let request = this.http.post(this.apiUrl + "users/create", {
                username: username,
                password: password,
                email: email
            });

            request.subscribe((response: any) => {

                loader.dismiss();

                if (response.message == "success") {
                    this.registerStatus = true;

                    this.createAuthAlert("Success", "Your account has been created", ['Nice']);
                    this.navCtrl.popToRoot();
                    this.createAuthAlert("Alright", "You can sign in now. (We won't do it for security reasons)", ["It's okay"]);

                }
            }, (errorResponse: HttpErrorResponse) => {
                loader.dismiss();

                if (errorResponse.status == 422) {
                    let errorMessage = "";

                    // @ts-ignore
                    Object.values(errorResponse.error.errors).forEach((error) => {
                        errorMessage += error
                    });

                    this.createAuthAlert("Whoops", errorMessage, ['Ok'])
                }
                else {

                }

            });

        }
    }

    loginUser(username, password, udi) {
        let loader = this.loadingCtrl.create({content: "Loading..", dismissOnPageChange: true});

        if (username.length < 1 || password.length < 1) {
            this.createAuthAlert("Error", "You have left some thing empty!", ['Okay'])
        }
        else {

            loader.present();

            let request = this.http.post(this.apiUrl + "users/login", {username: username, password: password, uniqueId: udi});

            request.subscribe((result: any) => {

                if(result.message == "success") {
                    this.loggedIn = true;

                    this.http.get(this.apiUrl + "users/" + result.token).subscribe((result: any) => {
                        this.storage.set('user', result).then(x => {
                            this.storage.set('token', result.token).then(x => {
                                this.navCtrl.setRoot(MessagesPage);
                            });
                        });
                    })


                    // this.storage.set('token', result.token).then(() => {
                    //     this.storage.set('user', this.getUser()).then(e => {
                    //
                    //     })
                    //
                    // });
                }

            }, (errorResult: HttpErrorResponse) => {
                loader.dismiss();
                if(errorResult.status == 422) {
                    this.createAuthAlert('Error', errorResult.error.message, ['Ok']);
                } else {
                    this.createAuthAlert('Error', errorResult.message, ['Ok']);
                }
            })


        }
    }

    fingerPrintLogin(udi) {

        let loader = this.loadingCtrl.create({content: "Authenticating..", dismissOnPageChange: true});
        loader.present();

        let request = this.http.post(this.apiUrl + "users/fingerprint", {deviceId: udi});

        request.subscribe((result: any) => {

            loader.dismiss();

            if(result.message == "success") {
                this.loggedIn = true;
                this.http.get(this.apiUrl + "users/" + result.token).subscribe((result: any) => {
                    this.storage.set('user', result).then(x => {
                        this.storage.set('token', result.token).then(x => {
                            this.navCtrl.setRoot(MessagesPage);
                        });
                    });
                })
            }

        }, (error: HttpErrorResponse) => {
            loader.dismiss();
            if(error.status == 404) {
                this.createAuthAlert('Error', "You have to login with your username and password first!", ['Ok']);
            } else if(error. status == 403) {
                this.createAuthAlert('Error', 'Your last session and current device differ too much. Please sign in with your username and password', ['Ok'])
            }
            else {
                this.createAuthAlert('Error', 'Cannot authenticate!', ['Ok']);
            }

        })

    }

    createAuthAlert(title: string, subtitle: string, buttons: string[]) {
        return this.alertCtrl.create({
            title: title,
            subTitle: subtitle,
            buttons: buttons
        }).present();
    }

}
