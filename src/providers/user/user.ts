import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Storage} from "@ionic/storage";
import {AlertController, NavController} from "ionic-angular";
import {HomePage} from "../../pages/home/home";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

    loggedIn: boolean = false;
    user: any = {};
    apiUrl: string = "https://api.boep.finleyhd.nl/api/";

    constructor(
        public http: HttpClient,
        private storage: Storage,
        private navCtrl: NavController,
        private uniqeId: UniqueDeviceID,
        private alertCtrl: AlertController,
    ) {

        this.storage.get('token').then((token => {
            if(token != null) {
                this.storage.get('user').then(user => {
                    if(user != null) {
                        this.loggedIn = true;
                        this.user = user;

                    }
                });
            }
        }));


    }

    logout() {
        this.storage.get('token').then((token) => {
            this.uniqeId.get().then((uuid) => {
                this.http.post(this.apiUrl + "users/logout", {
                    deviceId: uuid,
                    token: token
                }).subscribe((response: any) => {
                    if(response.message == "logout_ok") {
                        this.storage.remove('token').then(x => {
                            this.navCtrl.setRoot(HomePage);
                        })

                    }
                }, (error) => {})

            });
        })
    }

    createAuthAlert(title: string, subtitle: string, buttons: string[]) {
        return this.alertCtrl.create({
            title: title,
            subTitle: subtitle,
            buttons: buttons
        }).present();
    }

}
