import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {RegisterPage} from "../register/register";
import {LoginPage} from "../login/login";
import {MessagesPage} from "../messages/messages";
import {Storage} from "@ionic/storage";


@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {

    constructor(public navCtrl: NavController, private storage: Storage) {

        this.storage.get('token').then((token => {
            if(token != null) {
                this.navCtrl.setRoot(MessagesPage)
            }
        }));

    }

    openRegister() {
        this.navCtrl.push(RegisterPage);
    }

    openLogin() {
        this.navCtrl.push(LoginPage);
    }

}
