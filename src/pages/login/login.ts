import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {FingerprintAIO} from "@ionic-native/fingerprint-aio";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    providers: [AuthProvider]
})
export class LoginPage {

    username: string = "";
    password: string = "";
    fingerPrintAvailable: boolean = false;

    constructor(
        public navCtrl: NavController,
        public authProvider: AuthProvider,
        public fingerPrint: FingerprintAIO,
        public alertCtrl: AlertController,
        private uniqueId: UniqueDeviceID,
    ) {
        this.fingerPrint.isAvailable().then((res) => {
            this.fingerPrintAvailable = true;
            console.log(res);
        })
    }

    ionViewDidLoad() {
        console.log('Loaded loginpage');
    }

    login() {
        this.uniqueId.get().then((uuid) => {
            console.log('got id:' ,uuid);
            this.authProvider.loginUser(this.username, this.password, uuid);
        }, (error) => {

            this.authProvider.loginUser(this.username, this.password, this.uniqueId.get());

            this.alertCtrl.create({
                title: 'Error',
                subTitle: "Could not get unique device id: " + JSON.stringify(error),
                buttons: ['Ok']
            }).present();
        })

    }

    useFingerprint() {
        this.fingerPrint.show({
            clientId: 'boep-app',
            clientSecret: 'boepApp1998!',
            disableBackup: true,
            localizedFallbackTitle: 'Use pin',
            localizedReason: 'Log in using fingerprint'
        })
            .then((result) => {
                this.uniqueId.get().then(uuid => {
                    this.authProvider.fingerPrintLogin(uuid)
                });
            }, (error) => {
                console.log(error);
            })
    }

}
