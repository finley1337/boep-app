import {Component} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {NewMessagePage} from "../new-message/new-message";
import {AddBuddyPage} from "../add-buddy/add-buddy";
import {SettingsPage} from "../settings/settings";

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-messages',
    templateUrl: 'messages.html',
    providers: [UserProvider]
})
export class MessagesPage {

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public userProvider: UserProvider,
        public actionSheetCtrl: ActionSheetController,
        public modalCtrl: ModalController
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MessagesPage');
    }

    openUserPrefs() {
        this.actionSheetCtrl.create({
            title: this.userProvider.user.name,
            buttons: [
                {
                    text: 'Logout',
                    handler: () => {
                        this.userProvider.logout();
                    }
                }, {
                    text: 'Change avatar',
                    handler: () => {
                        this.navCtrl.push(SettingsPage);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        }).present()
    }

    newMessage() {
        this.modalCtrl.create(NewMessagePage).present();
    }

    addBuddy() {
        this.modalCtrl.create(AddBuddyPage).present();
    }

}
