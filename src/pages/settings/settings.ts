import {Component} from '@angular/core';
import {ActionSheetController, IonicPage, LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {Storage} from "@ionic/storage";
import {Camera} from "@ionic-native/camera";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html',
    providers: [UserProvider]
})
export class SettingsPage {

    user: User;
    avatar_url: string = "https://via.placeholder.com/100x100/dc143c/000000?text=loading";
    cameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.PNG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: 1,
    };

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private userProvider: UserProvider,
        private storage: Storage,
        public loadingCtrl: LoadingController,
        public actionSheetCtrl: ActionSheetController,
        public platform: Platform,
        private camera: Camera,
    ) {
    }

    loader = this.loadingCtrl.create({content: 'Loading..'});

    ionViewWillEnter() {
        this.loader.present();

        this.storage.get('user').then(user => {
            this.user = user;
            console.log(this.user);
            this.loader.dismiss();

            this.avatar_url = this.userProvider.apiUrl + "users/avatars/" + this.user.avatar;
            console.log('set avatar');
        });


    }

    ionViewDidLoad() {

    }

    changeAvatar() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Option',
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Take photo',
                    icon: !this.platform.is('ios') ? 'ios-camera-outline' : null,
                    handler: () => {
                        this.cameraOptions.sourceType = this.camera.PictureSourceType.CAMERA;
                        this.camera.getPicture(this.cameraOptions).then(imageData => {
                            this.avatar_url = 'data:image/png;base64,' + imageData;
                        }, () => this.userProvider.createAuthAlert('Oops', 'Camera is not availlable', ['Ok']));
                    }
                },
                {
                    text: 'Choose photo from Gallery',
                    icon: !this.platform.is('ios') ? 'ios-images-outline' : null,
                    handler: () => {
                        // @ts-ignore
                        this.cameraOptions.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;

                        this.camera.getPicture(this.cameraOptions).then(imageData => {
                            this.avatar_url = 'data:image/png;base64,' + imageData;
                        }, () => this.userProvider.createAuthAlert('Oops', 'Camera is not availlable', ['Ok']))
                    }
                },
            ]
        });
        actionSheet.present();
    }

}

