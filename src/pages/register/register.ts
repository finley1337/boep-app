import {Component} from '@angular/core';
import {AuthProvider} from "../../providers/auth/auth";
import {IonicPage, NavController} from "ionic-angular";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
    providers: [AuthProvider]
})
export class RegisterPage {
    username: string = "";
    password: string = "";
    email: string = "";


    constructor(
        public authProvider: AuthProvider,
        public navCtrl: NavController
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    register() {
        this.authProvider.registerUser(this.username, this.password, this.email);
    }


}
