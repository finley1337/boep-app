import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {RegisterPage} from "../pages/register/register";
import {HttpClientModule} from "@angular/common/http";
import {LoginPage} from "../pages/login/login";
import { AuthProvider } from '../providers/auth/auth';
import {MessagesPage} from "../pages/messages/messages";
import {IonicStorageModule} from "@ionic/storage";
import { UserProvider } from '../providers/user/user';
import {LoginPageModule} from "../pages/login/login.module";
import {MessagesPageModule} from "../pages/messages/messages.module";
import {RegisterPageModule} from "../pages/register/register.module";
import {NewMessagePageModule} from "../pages/new-message/new-message.module";
import {AddBuddyPageModule} from "../pages/add-buddy/add-buddy.module";
import {FingerprintAIO} from "@ionic-native/fingerprint-aio";
import {UniqueDeviceID} from "@ionic-native/unique-device-id";
import {SettingsPageModule} from "../pages/settings/settings.module";
import {Camera} from "@ionic-native/camera";


@NgModule({
    declarations: [
        MyApp,
        HomePage,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RegisterPageModule,
        LoginPageModule,
        MessagesPageModule,
        NewMessagePageModule,
        AddBuddyPageModule,
        SettingsPageModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        RegisterPage,
        LoginPage,
        MessagesPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        AuthProvider,
        UserProvider,
        FingerprintAIO,
        UniqueDeviceID,
        Camera,
    ]
})
export class AppModule {
}
